from weather import Weather, Unit
from pebble import concurrent, ProcessExpired
from concurrent.futures import TimeoutError
import threading
import os
import json
import socket
import struct
import psutil
import random
import sys
import subprocess
import datetime

local_bucket = {}
fog_bucket = {}
edge_bucket = {}



def sendata(location, function_name):
    """
    :param location: Coordinate values of the location in string format, Eg ""60.187760,24.825384""
    :param function_name: Name of the deployed serverless function in string format.
    Triggers the serverless function locally or at Edge layer or at Fog machine based on the condition.
    """
    time1 = datetime.datetime.now()
    global local_bucket
    global fog_bucket
    global edge_bucket
    #threading.Timer(5, sendata, args=[location, function_name]).start()
    w = Weather(unit=Unit.FAHRENHEIT)
    latitude = float([x.strip() for x in location.split(',')][0])
    longitude = float([x.strip() for x in location.split(',')][1])
    db_entry = db_value(w, latitude, longitude)

    cpu_utilization, memory_utilization, remaining_battery = threshold_values()
    # print(cpu_threshold, memory_threshold, remaining_battery)

    """
    whitelister() whitelists the function if it is in the dictionary for more than 2 hour.
    """
    if not bool(local_bucket):
        local_bucket = whitelister(local_bucket)

    if not bool(edge_bucket):
        edge_bucket = whitelister(edge_bucket)

    if not bool(fog_bucket):
        fog_bucket = whitelister(fog_bucket)

    """
    1. If function name exists in all of the buckets, move to Cloud.
	2. If function name exists in IoT-Gateway and Edge bucket, move to Fog layer.
	2. If fucntion name exists in IoT-Gateway bucket, move to Edge layer.
    """
    if function_name in local_bucket.values() and function_name in edge_bucket.values() \
            and function_name in fog_bucket.values():
        #print("inside all buckets")
        cloud_connect(db_entry, function_name)

    elif function_name in local_bucket.values() and function_name in edge_bucket.values() :
        #print("local bucket")
        edge(db_entry, function_name)

    elif function_name in local_bucket.values():
        #print("local bucket")
        edge(db_entry, function_name)

    elif function_name in edge_bucket.values():
        #print("edge bucket")
        fog(db_entry, function_name)

    elif function_name in fog_bucket.values():
        #print("fog bucket")
        cloud_connect(db_entry, function_name)

    elif cpu_utilization < 80 and memory_utilization < 80 and remaining_battery > 30:
        local = local_processing(db_entry, function_name)
        if local == b"{'statusCode': '200'}\n":
            print("Processed at local gateway")
        else:
            print(
                "Function exceeding threshold timing, moving to edge layer")

            """
            Add function name to the dictionary if timeout occurs and move to edge layer.

            """

            local_bucket[datetime.datetime.now()] = function_name
            #print("bucket entry")
            edge(db_entry, function_name)

    else:
        edge(db_entry, function_name)
    print(datetime.datetime.now() - time1)


def db_value(w, lat, long):
    """
    :param w: Object of class weather
    :param lat: latitude
    :param long: longitude
    :return: JSON value to write in MongoDB
    """

    weather = w.lookup_by_latlng(lat, long)
    atmosphere = weather.atmosphere
    condition = weather.condition
    if weather is None:
        value = {"Humidity": None,
                 "Temperature_farenheit": None,
                 "Temperature_celsius": None,
                 "Latitude": None,
                 "Longitude": None}
    else:
        tempfar = float(condition.temp)
        tempcel = (tempfar - 32) * float(5.0 / 9.0)
        value = {"Humidity": atmosphere['humidity'],
                 "Temperature_farenheit": tempfar,
                 "Temperature_celsius": tempcel,
                 "Latitude": lat,
                 "Longitude": long}
    return value


def whitelister(dictionary):
    """
    :param dictionary: Dictionary with datetime as key and serverless function name as value
    :return: Dictionary after removing a function name if condition satisfies.
    """
    for k in list(dictionary.keys()):
        if (datetime.datetime.now() > k) and (datetime.datetime.now().hour - k.hour) >= 2:
            dictionary.pop(k)
    return dictionary


def threshold_values():
    """
        cpu_util total cpu utilization of the machine/sensor.
        memory_util is the memory utilization of the machine/sensor.
        rem_battery is total power available. It is None is source has no battery but plugged to direct power
        source.
    :return: cpu_util, memory_util, rem_battery
    """
    cpu_util = psutil.cpu_percent(interval=1)

    memory_util = psutil.virtual_memory().percent

    # battery = psutil.sensors_battery()
    battery = None

    if battery is None:
        rem_battery = 100
    else:
        rem_battery = battery.percent
    return cpu_util, memory_util, rem_battery


def multicast_sender(ip, port):
    """
    Send multicast request to an IP and port.
    """
    message = "***"
    multicast_group = (ip, port)

    # Create the datagram socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Set a timeout so the socket does not block indefinitely when trying
    # to receive data.
    sock.settimeout(2)

    ttl = struct.pack('b', 1)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_DEFAULT_MULTICAST_TTL, ttl)
    matrix_dict = {}
    try:
        # print(sys.stderr, 'sending"%s"' % message)
        sock.sendto(message.encode('utf-8'), multicast_group)
        while True:
            # print('>>', sys.stderr, 'waiting to receive')
            try:
                data, server = sock.recvfrom(16)
            except socket.timeout:
                # print('>>', sys.stderr, 'timeout, no more responses')
                break
            else:
                # print('>>', sys.stderr, 'received "%s" from %s' % (data, server))
                matrix_dict[server[0]] = int(data)
        return matrix_dict
    finally:
        # print('>>', sys.stderr, 'closing socket')
        sock.close()


def local_processing(db_val, function_name):
    """
    :param db_val: JSON input to write in mongo DB
    :param function_name: Name of the deployed serverless function in string format.
    Function triggers the serverless function hosted on the local machine.
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('IoT-gateway_ip_address', 8080))
    local_output = subprocess.check_output(
        "curl IoT-gateway_ip_address:8080/function/{0} --data-binary '{1}'".format(function_name, json.dumps(db_val)), shell=True)
    return local_output


def edge_connect(db_val, function_name):
    """
       :param db_val: JSON input to write in mongo DB
       :param function_name: Name of the deployed serverless function in string format.
       Function triggers the serverless function hosted on the Edge machine.
    """

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect(('edge_ip_address', 8080))

        edge_output = subprocess.check_output(
            "curl edge_ip_address:8080/function/{0} --data-binary '{1}'".format(function_name, json.dumps(db_val)),
            shell=True)
        return edge_output
    except socket.error as e:
        print("Error on connect on edge machine: {0}".format(e))
        s.close()


def fog_connect(db_val, function_name):
    """
        :param db_val: JSON input to write in mongo DB
        :param function_name: Name of the deployed serverless function in string format.
        Function triggers the serverless function hosted on the Fog layer.
        In case fog layer is not available, function is sent to cloud machine.
    """

    matrix = multicast_sender('224.3.29.71', 10000)
    print(matrix)
    if bool(matrix):
        if min(matrix.values()) == 0:
            if all(value == next(iter(matrix.values())) for value in matrix.values()):
                ip_address = random.choice(list(matrix))
            else:
                ip_address = max(matrix, key=matrix.get)
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            try:
                url = "curl " + ip_address + ":8080/function/{0} --data-binary '{1}'"
                s.connect((ip_address, 8080))
                fog_output = subprocess.check_output(
                    url.format(function_name, json.dumps(db_val)),
                    shell=True)
                return fog_output
            except socket.error as e:
                print("Error connecting function on pi : {0}, connecting cloud".format(e))
                s.close()
                cloud_connect(db_val, function_name)
        else:
            cloud_connect(db_val, function_name)
    else:
        cloud_connect(db_val, function_name)


def cloud_connect(db_val, function_name):
    """
       :param db_val: JSON input to write in mongo DB
       :param function_name: Name of the deployed serverless function in string format.
       Function triggers the serverless function hosted at the IBM cloud.
    """
    try:
        cloud_output = subprocess.check_output(
            "bx wsk action invoke {0} --result".format(function_name),
            shell=True)
        return cloud_output
    except:
        print("Unable to connect to Cloud")


def fog(db, func):
    """
    :param db: JSON input to write in mongo DB
    :param func: Serverless function name
    Connect Edge if timeout occurs at Fog layer.
    """
    fogg = fog_connect(db, func)
    if fogg == b"{'statusCode': '200'}\n":
        print("Processed at fog gateway")
    else:
        print(
            "Function exceeding threshold timing, moving to cloud layer")

        """
        Add function name to the dictionary if timeout occurs and move to cloud layer.
        """

        fog_bucket[datetime.datetime.now()] = func
        cloudC = cloud_connect(db, func)
        if cloudC == b'{\n    "statusCode": "200"\n}\n':
            print("Request has been processed at the cloud")


def edge(db, func):
    """
    :param db: JSON input to write in mongo DB
    :param func: Serverless function name
    Connect cloud if timeout occurs at Fog layer.
    """
    ede = edge_connect(db, func)
    if ede == b"{'statusCode': '200'}\n":
        print("Processed at Edge gateway")
    else:
        print(
            "Function exceeding threshold timing, moving to fog layer")

        """
        Add function name to the dictionary if timeout occurs and move to cloud layer.
        """

        edge_bucket[datetime.datetime.now()] = func
        fog(db, func)


if __name__ == '__main__':
    sendata("60.187760,24.825384", 'mongowriter')
