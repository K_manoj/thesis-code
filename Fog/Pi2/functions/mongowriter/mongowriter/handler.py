from pymongo import MongoClient
from datetime import datetime
import json
import time


def handle(req):
    client = MongoClient("mongodb://10.10.10.10:27017")
    db = client.sensor
    json_req = json.loads(req)
    #time.sleep(50)
    db_entry = {"Humidity": json_req["Humidity"],
                "Temperature_farenheit": json_req["Temperature_farenheit"],
                "Temperature_celsius": json_req["Temperature_celsius"],
                "Latitude": json_req["Latitude"],
                "Longitude": json_req["Longitude"],
                "Time": datetime.now()}
    db.sensordata.insert(db_entry)
    #print("\n Data entered in DB at {0} using pi2".format(datetime.now()))
    return {"statusCode": "200"}
