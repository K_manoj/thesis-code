import socket
import struct
import sys
import psutil
import subprocess

multicast_group = '224.3.29.71'

server_address = ('', 10000)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.bind(server_address)

group = socket.inet_aton(multicast_group)
mreq = struct.pack('4sL', group, socket.INADDR_ANY)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

while True:
    print('>>', sys.stderr, '\nwaiting to receive message')
    data, address = sock.recvfrom(1024)
    print('>>', sys.stderr, 'received %s bytes from %s' % (len(data), address))
    print('>>', sys.stderr, data)
    print('>>', sys.stderr, 'sending acknowledgement to', address)
    '''cpu_available = 100 - psutil.cpu_percent(interval=0.1)
    free_mem = 100 - psutil.virtual_memory().percent
    battery = psutil.sensors_battery()

    if battery is None:
        remaining_battery = 100
    else:
        remaining_battery = battery.percent
    matrix = str(min(cpu_available, free_mem, remaining_battery))
    '''
    proc = subprocess.Popen(["docker node ls --format '{{.Availability}}'"], stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE, shell=True)
    active_nodes = str(list(proc.communicate()).count(b'Active\n'))
    print(active_nodes)

    sock.sendto(active_nodes, address)
