# Thesis code

This project contains the code of my master's thesis on "Serverless computing for the Internet of Things".

## Getting Started

The setup contains four layers IoT gayeway, Edge, Fog and Cloud layers.  

### Prerequisites

The basic setup requires 4 raspberry pi 3 devices, a Ubuntu 16.04 LTS machine and IBM cloud login. We also need a power source and a LAN switch along with ethernet cables. We have used python for this project.  


### Installation

Group 3 Respberry Pi 3 devices in Docker swarm mode. Ubuntu 16.04 LTS machine and 4th raspberry pi also require Docker installation in swarm mode individually.  
For our setup Pi 1, 2 and 4 are grouped in Docker swarm mode and serve as Fog layers. Pi 3 serves as the IoT-Gateway while Ubuntu 16.04 LTS acts as Edge machine.  
FaaS can be deployed on IOT-Gateway, Edge and Fog layers by using instruction given in the [blog](https://blog.alexellis.io/your-serverless-raspberry-pi-cluster/).  

Deploy function on IBM cloud functions to serve as a the Cloud layer.  

Install and run MongoDB on a machine. Function uses the Pymongo to connect the MongoDB.  


### Building and deploying the function on OpenFaaS

 faas-cli deploy -f ./function-name.yml  
 faas-cli build -f ./function-name.yml  

 
### Building and deploying the function on IBM functions

bx wsk action create function-name code-file.py  
bx wsk action invoke function-name --result  

The  faas folder in the repository will be replaced by generated OpenfaaS build. We need to generate the OpenfaaS project skeleton and replace handler.py, requirement.txt and "function-name".yml files before building the function.  
Similarly, we also need to create IBM cloud function with the given code.  

Note - Make sure to use correct IP addresses before building the function.  


### Results

Sensor.py triggers the OpenFaaS function from IoT-Gateway. The Result.txt file contains results obtained for individual cases for 10 iterations.
